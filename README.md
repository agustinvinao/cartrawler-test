## Install dependencies
_Im personal use yarn, but you can use npm if you like_

```
yarn install
```

### Run the project for dev:

```
yarn dev
```

### Run project's tests:

```
yarn jest
```

## Features implemented:

* List of cars
* List by vendor
* Show specific card
* Sort by different fields (ascending and descending)

## Tools used:

* Webpack (to handle all the libraries, files, build, etc.)
* Babel (to use new features on JS)
* Typescript (Provides more knowledge of your code every time you write more.)
* ESLint 
* SASS
* JEST (An excellent, ready to use, test library. With powerful mocking facilities and integrated tools like code coverage out of the box.)

## deployment

I've choose heroku based on the simplicity of this service. You'll see a file named Procfile, this is require to inform heroku what they need to run to execute the service you're deploying. 

If you want to deploy this to a new heroku account the steps are quite simple:

1. create your account on heroku
2. download and install heroku-cli
3. log in on your terminal to heroku
4. create the project on heroku (this will show you how to add the remote git repository you'll use for your deployments)
push to heroku's remote repository
5. Any time you push to heroku it will redeploy the app and start it using the command on Procfile.

### Url on heroku:
[https://cartrawler.herokuapp.com/](https://cartrawler.herokuapp.com/)


Heroku has really good docs https://devcenter.heroku.com/articles/getting-started-with-nodejs#deploy-the-app
