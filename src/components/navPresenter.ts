export const navBar = (): string => {
  return `
    <ul class="nav-bar">
      <li><button route="/">All</button></li>
      <li><button route="#/group-by-vendor">Group by Vendor</button></li>
    </ul>
  `
}