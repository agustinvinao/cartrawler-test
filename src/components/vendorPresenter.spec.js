import {getHTMLHeader} from './vendorPresenter'
import Vendor from './../classes/vendor'
jest.mock('./../classes/vendor')

beforeEach(() => {
  // Clear all instances and calls to constructor and all methods:
  Vendor.mockClear();
});

test('#viewVehicle', () => {
  const vendor = new Vendor();
  const gethtmlheader = getHTMLHeader(vendor);
  expect(typeof gethtmlheader).toBe('string');
})