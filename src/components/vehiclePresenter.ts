import Vehicle from './../classes/vehicle'

export const getCard = (vehicle: Vehicle): string => {
  return `
  <div class="vehicle--card">
    <img src="${vehicle.PictureURL}" />
    <ul>
      <li><span>Total: </span>${vehicle.CurrencyCode} ${
    vehicle.RateTotalAmount
  }</li>
      <li><span>Code: </span>${vehicle.Code}</li>
      <li><span>Door Count: </span>${vehicle.DoorCount}</li>
      <li><span>Model Name: </span>${vehicle.ModelName}</li>
      <li><span>Passenger Quality: </span>${vehicle.PassengerQuantity}</li>
      <li><span>Status: </span>${vehicle.Status}</li>
    </ul>
    <button route="#/car/${vehicle.Code}">View Details</button>
  </div>
  `;
};

export const viewVehicle = (vehicle: Vehicle): string => {
  return `
  <div class="vehicle--view">
    <img src="${vehicle.PictureURL}" />
    <div class="vehicle--view-details">
      <div><div>Total:</div><div>${vehicle.CurrencyCode} ${
    vehicle.RateTotalAmount
  }</div></div>
      <div><div>Air Condition: </div><div>${vehicle.AirConditionInd}</div></div>
      <div><div>Baggage Quality:</div><div>${
        vehicle.BaggageQuantity
      }</div></div>
      <div><div>Code:</div><div>${vehicle.Code}</div></div>
      <div><div>Code Context:</div><div>${vehicle.CodeContext}</div></div>
      <div><div>Door Count:</div><div>${vehicle.DoorCount}</div></div>
      <div><div>Driver Type:</div><div>${vehicle.DriveType}</div></div>
      <div><div>Fuel Type:</div><div>${vehicle.FuelType}</div></div>
      <div><div>Model Name:</div><div>${vehicle.ModelName}</div></div>
      <div><div>Passenger Quality:</div><div>${
        vehicle.PassengerQuantity
      }</div></div>
      <div><div>Status:</div><div>${vehicle.Status}</div></div>
    </div>
  </div>
  `;
};
