import Vendor from './../classes/vendor'

export const getHTMLHeader = (vendor: Vendor): string => {
  return `
  <div class="vendor--header">
    <div>${vendor.Name} <small>${vendor.Code}</small></div>    
  </div>
  `;
};
