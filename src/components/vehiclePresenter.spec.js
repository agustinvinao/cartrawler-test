import {getCard, viewVehicle} from './vehiclePresenter'
import Vehicle from './../classes/vehicle'
jest.mock('./../classes/vehicle')

beforeEach(() => {
  // Clear all instances and calls to constructor and all methods:
  Vehicle.mockClear();
});

test('#getCard', () => {
  const vehicle = new Vehicle();
  const getcard = getCard(vehicle);
  expect(typeof getcard).toBe('string');
})

test('#viewVehicle', () => {
  const vehicle = new Vehicle();
  const viewvehicle = viewVehicle(vehicle);
  expect(typeof viewvehicle).toBe('string');
})