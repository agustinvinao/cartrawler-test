import { getCard } from "./vehiclePresenter";
import { getHTMLHeader } from "./vendorPresenter";
import { dateFormat } from "../lib/helpers";
import RentalPoint from "./../classes/rentalPoint";
import Vehicle from "./../classes/vehicle";
import Vendor from "./../classes/vendor";

export const allVendorsVehicleList = (rentalPoint: RentalPoint): string => {
  return `
    <div class="vendor--sortby">
      ${sortByBar(rentalPoint)}
    </div>
    <div class="vendor--vehicles">
      ${rentalPoint
        .getAllVehiclesByPrice()
        .map((vehicle: Vehicle) => getCard(vehicle))
        .join("")}
    </div>
  `;
};

export const byVendorVehicleList = (rentalPoint: RentalPoint): string => {
  return `
    ${rentalPoint.vendors
      .map(
        (vendor: Vendor) =>
          '<div class="vendor">' +
          getHTMLHeader(vendor) +
          '<div class="vendor--vehicles">' +
          vendor
            .getVehiclesByPrice()
            .map((vehicle: Vehicle) => getCard(vehicle))
            .join("") +
          "</div>" +
          "</div>"
      )
      .join("")}  
  `;
};

export const sortByBar = (rentalPoint: RentalPoint): string => {
  let selected = rentalPoint.getSortCriteria();
  let direction = "asc";

  if (selected[0] === "-") {
    selected = selected.slice(1, selected.length);
    direction = "desc";
  }

  return `
    Sort list by: 
    <form id="sort-vehicles">
      <select name="field">
        ${[
          "BaggageQuantity",
          "Code",
          "DoorCount",
          "DriveType",
          "DriveType",
          "FuelType",
          "ModelName",
          "PassengerQuantity",
          "Status",
          "TransmissionType",
          "RateTotalAmount"
        ]
          .map(
            (name: string) =>
              `<option value="${name}" ${
                selected === name ? 'selected="selected"' : ""
              }>${name}</option>`
          )
          .join("")}
      </select>
      <select name="direction">
        <option value="asc" ${
          direction === "asc" ? 'selected="selected"' : ""
        }>Asc</option>
        <option value="desc" ${
          direction === "desc" ? 'selected="selected"' : ""
        }>Desc</option>
      </select>
      <button type="submit">Update</button>
    </form>
  `;
};

export const getHTMLLegend = (rentalPoint: RentalPoint): string => {
  return `
    <div class="rental-point--schedule">
      <div><span>PickUp</span> ${rentalPoint.PickUpLocation} - ${dateFormat(
    rentalPoint.PickUpAt
  )}</div>
      <div><span>Return</span> ${rentalPoint.ReturnLocation} - ${dateFormat(
    rentalPoint.ReturnAt
  )}</div>
    </div>
  `;
};
