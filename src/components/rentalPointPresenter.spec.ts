import {
  allVendorsVehicleList,
  byVendorVehicleList,
  sortByBar,
  getHTMLLegend
} from "./rentalPointPresenter";
import RentalPoint from "./../classes/rentalPoint";

const carsJson = require("./../cars.json");
const rentalPoints: Array<RentalPoint> = carsJson.map(
  (rentalPointData: string) => new RentalPoint(rentalPointData)
);

test("#allVendorsVehicleList", () => {
  const rentalPoint = rentalPoints[0];
  const result = allVendorsVehicleList(rentalPoint);
  expect(typeof result).toEqual("string");
});

test("#byVendorVehicleList", () => {
  const rentalPoint = rentalPoints[0];
  const result = byVendorVehicleList(rentalPoint);
  expect(typeof result).toEqual("string");
});

test("#sortByBar", () => {
  const rentalPoint = rentalPoints[0];
  const result = sortByBar(rentalPoint);
  expect(typeof result).toEqual("string");
});

test("#getHTMLLegend", () => {
  const rentalPoint = rentalPoints[0];
  const result = getHTMLLegend(rentalPoint);
  expect(typeof result).toEqual("string");
});
