import express from 'express'
const server = express()
const expressStaticGzip = require("express-static-gzip")
import webpack from 'webpack'

import configDevClient  from '../../config/webpack.dev-client'
import configProdClient  from '../../config/webpack.prod-client'

const isProd = process.env.NODE_ENV === "production"
const isDev = !isProd


if(isDev) {
  const compiler = webpack([configDevClient])
  const devCompiler = compiler.compilers[0]
  // webpack middleware to use devServer config
  const webpackDevMiddleware = require("webpack-dev-middleware")(
    devCompiler,
    devCompiler.devServer
  )
  server.use(webpackDevMiddleware)
  // webpack hot reload (this needs to go after devMiddleware and before static middleware)
  const webpackHotMiddleware = require('webpack-hot-middleware')(devCompiler)
  
  server.use(webpackHotMiddleware)
  console.log("Middleware enabled")

} else {
  webpack([configProdClient]).run((err, stats) => {    
    server.use(
      expressStaticGzip("dist", {
        enableBrotli: true
      })
    )
  })
}


const PORT = process.env.PORT || 8000
server.listen(PORT, () => {
  console.log(`Server is listening on http://localhost:${PORT}`)
})