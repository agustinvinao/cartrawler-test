import { dateFormat, dynamicSort } from "./helpers";

test("#dateFormat", () => {
  const now = new Date();
  expect(dateFormat(now)).toEqual(
    now.toLocaleDateString(undefined, {
      day: "numeric",
      month: "short",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit"
    })
  );
});

test("#dynamicSort", () => {
  const list = [ {"a": 1}, {"a": 2}, {"a": 3} ]
  let property = "a";
  let sortFn = dynamicSort(property)
  expect(list.sort(sortFn)).toEqual(list);
  property = "-a";
  sortFn = dynamicSort(property);
  expect(list.sort(sortFn)).toEqual([ {"a": 3}, {"a": 2}, {"a": 1} ]);
});
