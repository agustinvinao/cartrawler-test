import Vehicle from "./vehicle";
import { IVendor } from "./interfaces";
import { dynamicSort } from "./../lib/helpers";

export default class Vendor implements IVendor {
  Code: string;
  Name: string;
  VehicleAvailables: Array<Vehicle>;

  constructor(data: Object) {
    this.Code = data["Vendor"]["@Code"];
    this.Name = data["Vendor"]["@Name"];
    this.VehicleAvailables = data["VehAvails"].map(
      vehicleData => new Vehicle(vehicleData)
    );
  }

  getVehiclesByPrice(sortBy: string = "RateTotalAmount"): Array<Vehicle> {
    return this.VehicleAvailables.sort(dynamicSort(sortBy));
  }
}
