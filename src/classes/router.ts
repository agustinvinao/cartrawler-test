import { IRoute, IRouter } from "./interfaces";
import Route from "./route";
import RentalPoint from "./rentalPoint";
export default class Router implements IRouter {
  name: string;
  routes: Array<Route>;
  rentalPoint: RentalPoint;
  listeners: Array<Object>;

  constructor(name: string, routes: Array<Route>, rentalPoint: RentalPoint) {
    this.name = name;
    this.routes = routes;
    this.rentalPoint = rentalPoint;
    this.listeners = [];
  }

  render(currentPath: string, viewEl): void {
    const currentRoute = this.getRoute(currentPath);
    if (currentRoute) {
      viewEl.innerHTML = currentRoute.fn(this.rentalPoint);
    } else {
      viewEl.innerHTML = "<div>Route not found</div>";
    }
    this.registerRouteListeners();
  }

  navigateTo(event, view) {
    var routeInfo = this.getRoute(event.target.attributes[0].value);
    if (!routeInfo) {
      window.history.pushState({}, "", "error");
      view.innerHTML = "No route exists with this path";
    } else {
      const regexpCar = new RegExp(/\#\/car\/(.+)/);
      const path = event.target.attributes[0].value.match(regexpCar)
        ? event.target.attributes[0].value
        : routeInfo.path;
      window.history.pushState({}, "", path);
      view.innerHTML = routeInfo.fn(this.rentalPoint);
      if (path === "/") {
        this.registerSubmitSort(view, routeInfo);
      }
      this.registerRouteListeners();
    }
  }

  getRoute(path: string): Route {
    const hash = path;
    const regexpCar = new RegExp(/\#\/car\/(.+)/);
    path = hash.match(regexpCar) ? "#/car/:code" : path;
    return this.routes.filter((route: IRoute) => route.path === path)[0];
  }

  registerRouteListeners(
    contentEl: HTMLElement = document.getElementById("content")
  ): void {
    const clickHandler = event => this.navigateTo(event, contentEl);
    document.querySelectorAll("[route]").forEach((route: Node) => {
      route.addEventListener("click", clickHandler, false);
    });
  }

  registerSubmitSort(view: HTMLElement, route: Route) {
    const submitHandler = event => {
      event.preventDefault();
      this.rentalPoint.setSortCriteria(
        `${event.target[1].value === "desc" ? "-" : ""}${event.target[0].value}`
      );
      view.innerHTML = route.fn(this.rentalPoint);
      this.registerSubmitSort(view, route);
      this.registerRouteListeners();
    };
    document
      .getElementById("sort-vehicles")
      .addEventListener("submit", submitHandler, false);
  }
}
