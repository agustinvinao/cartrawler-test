import RentalPoint from './rentalPoint'
import Vehicle from './vehicle'

const carsJson = require("./../cars.json");

describe("#Vendor", () => {
  const rentalPoints: Array<RentalPoint> = carsJson.map(
    (rentalPointData: string) => new RentalPoint(rentalPointData)
  );
  const vendor = rentalPoints[0].vendors[0];
  test("#getVehiclesByPrice", () => {
    let vehiclesCodes = vendor
      .getVehiclesByPrice()
      .map((vehicle: Vehicle) => vehicle.Code);
    expect(vehiclesCodes).toEqual(["PCAR", "IFAR"]);
    vehiclesCodes = vendor
      .getVehiclesByPrice("Code")
      .map((vehicle: Vehicle) => vehicle.Code);
    expect(vehiclesCodes).toEqual(["IFAR", "PCAR"]);
  });
});
