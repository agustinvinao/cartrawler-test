import { dynamicSort } from "./../lib/helpers";
import { IRentalPoint } from "./interfaces";
import Vendor from "./vendor";
import Vehicle from "./vehicle";

export default class RentalPoint implements IRentalPoint {
  PickUpAt: Date;
  ReturnAt: Date;
  PickUpLocation: string;
  ReturnLocation: string;
  SortCriteria: string;
  vendors: Array<Vendor>;

  constructor(data: Object) {
    const core = data["VehAvailRSCore"]["VehRentalCore"];
    this.PickUpAt = new Date(core["@PickUpDateTime"]);
    this.PickUpLocation = core["PickUpLocation"]["@Name"];
    this.ReturnAt = new Date(core["@ReturnDateTime"]);
    this.ReturnLocation = core["PickUpLocation"]["@Name"];
    this.SortCriteria = "-RateTotalAmount";
    this.vendors = data["VehAvailRSCore"]["VehVendorAvails"].map(
      (vendorData: Object) => new Vendor(vendorData)
    );
  }

  getAllVehiclesByPrice(sortBy: string = this.SortCriteria): Array<Vehicle> {
    return []
      .concat(...this.vendors.map((vendor: Vendor) => vendor.VehicleAvailables))
      .sort(dynamicSort(sortBy));
  }

  setSortCriteria(newSortCriteria: string): void {
    this.SortCriteria = newSortCriteria;
  }

  getSortCriteria(): string {
    return this.SortCriteria;
  }

  getVehicle(code: string): Vehicle {
    return []
      .concat(...this.vendors.map((vendor: Vendor) => vendor.VehicleAvailables))
      .filter((vehicle: Vehicle) => vehicle.Code === code)[0];
  }
}
