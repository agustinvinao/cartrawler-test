import Vendor from './vendor'
import Vehicle from './vehicle'
import RentalPoint from './rentalPoint'

export enum ECodeContextType {
  "CARTRAWLER"
}
export enum ECurrencyCodeType {
  "CAD",
  "USD",
  "EUR"
}
export enum EDriveType {
  "Unspecified"
}
export enum EFuelType {
  "Petrol"
}
export enum EStatusType {
  "Available",
  "Not Available"
}
export enum ETrasmissionType {
  "Automatic",
  "Manual"
}

export interface IRoute {
  name: string;
  path: string;
  fn: (rentalPoint: RentalPoint) => string;
}

export interface IRouter {
  name: string;
  routes: Array<IRoute>;
}

export interface IVehicle {
  AirConditionInd: boolean;
  BaggageQuantity: number;
  Code: string;
  CodeContext: ECodeContextType;
  DoorCount: number;
  DriveType: EDriveType;
  FuelType: EFuelType;
  ModelName: string;
  PassengerQuantity: string;
  PictureURL: string;
  Status: EStatusType;
  TransmissionType: ETrasmissionType;

  CurrencyCode: ECurrencyCodeType;
  EstimatedTotalAmount: number;
  RateTotalAmount: number;
}

export interface IVendor {
  Code: string;
  Name: string;
  VehicleAvailables: Array<Vehicle>;
}

export interface IRentalPoint {
  PickUpAt: Date;
  PickUpLocation: string;
  ReturnAt: Date;
  ReturnLocation: string;
  SortCriteria: string;
  vendors: Array<Vendor>;
}
