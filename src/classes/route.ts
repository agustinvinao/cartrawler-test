import { IRoute } from "./interfaces";
import RentalPoint from "./rentalPoint";

export default class Route implements IRoute {
  name: string;
  path: string;
  fn: (rentalPoint: RentalPoint) => string;

  constructor(
    path: string,
    name: string,
    fn: (rentalPoint: RentalPoint) => string
  ) {
    this.name = name;
    this.path = path;
    this.fn = fn;
  }
}
