import RentalPoint from './rentalPoint'
import Vendor from './vendor'
import { dynamicSort } from "./../lib/helpers";
const carsJson = require("./../cars.json");


describe("#RentalPoint", () => {
  let rentalPoint;
  beforeEach(() => {
    const rentalPoints: Array<RentalPoint> = carsJson.map(
      (rentalPointData: string) => new RentalPoint(rentalPointData)
    );
    rentalPoint = rentalPoints[0];
  });

  test("#setSortCriteria", () => {
    expect(rentalPoint.getSortCriteria()).toEqual("-RateTotalAmount");
    rentalPoint.setSortCriteria("Code");
    expect(rentalPoint.getSortCriteria()).toEqual("Code");
  });

  test("#getSortCriteria", () => {
    expect(rentalPoint.getSortCriteria()).toEqual("-RateTotalAmount");
  });

  test("#getVehicle", () => {
    const vehicle = rentalPoint.vendors[0].VehicleAvailables[0];
    expect(rentalPoint.getVehicle(vehicle.Code)).toEqual(vehicle);
  });

  test("#getAllVehiclesByPrice", () => {
    const vehicles = []
      .concat(
        ...rentalPoint.vendors.map((vendor: Vendor) => vendor.VehicleAvailables)
      )
      .sort(dynamicSort("-RateTotalAmount"));
    expect(rentalPoint.getAllVehiclesByPrice()).toEqual(vehicles);
  });
});