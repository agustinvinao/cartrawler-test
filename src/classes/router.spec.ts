const carsJson = require("./../cars.json");
import RentalPoint from './rentalPoint'
import Route from './route'
import Router from './router'



describe('#Router', () => {
  const fn = (rentalPoint: RentalPoint): string => { return 'test'}
  const rentalPoints: Array<RentalPoint> = carsJson.map(
    (rentalPointData: string) => new RentalPoint(rentalPointData)
  );
  const router = new Router(
    'router',
    [
      new Route( "/", "Root", fn),
      new Route( "/route1", "Route1", fn),
      new Route( "/route2", "Route2", fn)
    ],
    rentalPoints[0]
  )
  // test('#render', () => {
  //   // Object.defineProperty(global, 'document', {
  //   //   writable: true,
  //   //   value: 'someurl'
  //   // });
  //   // getElementById
  //   // const mock = jest.fn().mockImplementation(() => {
  //   //   return {playSoundFile: mockPlaySoundFile};
  //   // });
  //   // let viewEl = {innerHTML: ''}
  //   // router.render('/route1', viewEl)
  //   // expect(viewEl.innerHTML).toEqual('test')
  // })
  // test('#navigateTo', () => {})
  test('#getRoute', () => {
    const route = router.routes[1]
    expect(router.getRoute('/route1')).toEqual(route)
  })
  // test('#registerRouteListeners', () => {})
  // test('#registerSubmitSort', () => {})
})