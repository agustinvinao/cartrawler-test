import {
  IVehicle,
  ECodeContextType,
  ECurrencyCodeType,
  EDriveType,
  EFuelType,
  EStatusType,
  ETrasmissionType
} from "./interfaces";

export default class Vehicle implements IVehicle {
  AirConditionInd: boolean;
  BaggageQuantity: number;
  Code: string;
  CodeContext: ECodeContextType;
  DoorCount: number;
  DriveType: EDriveType;
  FuelType: EFuelType;
  ModelName: string;
  PassengerQuantity: string;
  PictureURL: string;
  Status: EStatusType;
  TransmissionType: ETrasmissionType;
  CurrencyCode: ECurrencyCodeType;
  EstimatedTotalAmount: number;
  RateTotalAmount: number;

  constructor(data: Object) {
    const vehicleData = data["Vehicle"];
    const charge = data["TotalCharge"];
    this.AirConditionInd = vehicleData["@AirConditionInd"];
    this.BaggageQuantity = parseInt(vehicleData["@BaggageQuantity"]);
    this.Code = vehicleData["@Code"];
    this.CodeContext = vehicleData["@CodeContext"];
    this.CurrencyCode = charge["@CurrencyCode"];
    this.DoorCount = parseInt(vehicleData["@DoorCount"]);
    this.DriveType = vehicleData["@DriveType"];
    this.EstimatedTotalAmount = parseFloat(charge["@EstimatedTotalAmount"]);
    this.FuelType = vehicleData["@FuelType"];
    this.ModelName = vehicleData["VehMakeModel"]["@Name"];
    this.PassengerQuantity = vehicleData["@PassengerQuantity"];
    this.PictureURL = vehicleData["PictureURL"];
    this.RateTotalAmount = parseFloat(charge["@RateTotalAmount"]);
    this.Status = data["@Status"];
    this.TransmissionType = vehicleData["@TransmissionType"];
  }
}
