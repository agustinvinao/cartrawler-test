import "./css/main.scss";
import Route from './classes/route'
import RentalPoint from './classes/rentalPoint'
import Router from './classes/router'

import * as rentalPointPresenter from "./components/rentalPointPresenter";
import * as vehiclePresenter from "./components/vehiclePresenter";
import * as navBarPresenter from "./components/navPresenter";

console.log(`environment is ${process.env.NODE_ENV}`);

const carsJson = require("./cars.json");

const rentalPoints: Array<RentalPoint> = carsJson.map(
  (rentalPointData: string) => new RentalPoint(rentalPointData)
);

const routeRoot = new Route(
  "/",
  "Root",
  rentalPointPresenter.allVendorsVehicleList
);
const _router = new Router(
  "_router",
  [
    routeRoot,
    new Route(
      "#/group-by-vendor",
      "GroupByVendor",
      rentalPointPresenter.byVendorVehicleList
    ),
    new Route("#/car/:code", "CarView", (rentalPoint: RentalPoint) => {
      const hash = document.location.hash;
      const regexpCar = new RegExp(/\#\/car\/(.+)/);
      if (hash.match(regexpCar)) {
        const vehicle = rentalPoint.getVehicle(hash.match(regexpCar)[1]);
        return vehiclePresenter.viewVehicle(vehicle);
      }
      return "card code not found";
    })
  ],
  rentalPoints[0]
);

document.getElementById(
  "legend"
).innerHTML = rentalPointPresenter.getHTMLLegend(rentalPoints[0]);
document.getElementById("header").innerHTML = navBarPresenter.navBar();
const contentEl = document.getElementById("content");
_router.render("/", contentEl);
_router.registerSubmitSort(contentEl, routeRoot);
