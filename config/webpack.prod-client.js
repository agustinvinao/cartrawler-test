const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const minifyPlugin = require("mini-css-extract-plugin");
const compressionPlugin = require("compression-webpack-plugin");
const brotliPlugin = require("brotli-webpack-plugin");
const htmlWebpackPlugin = require("html-webpack-plugin");
// const BundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
module.exports = {
  name: "client",
  entry: {
    main: ["./src/main"]
  },
  resolve: {
    extensions: [".js", ".ts"]
  },
  mode: "production",
  output: {
    filename: "[name]-bundle-[hash:8].js",
    path: path.resolve(__dirname, "../dist"),
    publicPath: "/"
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          name: "vendor",
          chunks: "initial",
          minChunks: 2
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: "/.js$/",
        use: [
          {
            loader: "babel-loader",
            options: { babelrc: true }
          }
        ],
        exclude: /node_modules/
      },
      { test: /\.tsx?$/, loader: "ts-loader" },
      {
        test: /\.css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: "css-loader" },
          { loader: "postcss-loader" }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: "css-loader" },
          { loader: "postcss-loader" },
          { loader: "sass-loader" }
        ]
      },
      {
        test: /\.html$/,
        use: [{ loader: "html-loader", options: { attrs: ["img:src"] } }]
      },
      {
        test: /\.(png|jpg)$/,
        use: [
          {
            loader: "file-loader",
            options: { name: "images/[name]-[hash:8].[ext]" }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name]-[contenthash].css"
    }),
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.css$/g,
      cssProcessor: require("cssnano"),
      cssProcessorOptions: { discardComments: { removeAll: true } },
      canPrint: true
    }),
    new htmlWebpackPlugin({
      template: "./src/index.html",
      inject: true
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
        WEBPACK: true
      }
    }),
    new minifyPlugin(),
    new compressionPlugin({ algorithm: "gzip" }),
    new brotliPlugin()
    // new BundleAnalyzer({
    //   generateStatsFile: true
    // })
  ]
};
