const path = require('path')
const webpack = require('webpack')
const htmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  name: "client",
  entry: {
    main: [
      'babel-runtime/regenerator',
      'webpack-hot-middleware/client?reload=true',
      './src/main'
    ],
  },
  resolve: {
    extensions: [".js", ".ts"]
  },
  mode: 'development',
  output: {
    filename: '[name]-bundle.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: "/"
  },
  devServer: {
    historyApiFallback: true,
    contentBase: "dist",
    overlay: true,
    stats: {
      colors: true
    },
    hot: true
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: '/\.js$/',
        use: [
          {
            loader: 'babel-loader',
            options: { babelrc: true },
          }
        ],
        exclude: /node_modules/
      },
      { test: /\.tsx?$/, loader: "ts-loader" },
      { 
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader', 'postcss-loader' ]
      },
      { 
        test: /\.scss$/,
        use: [ 'style-loader', 'css-loader', 'postcss-loader', 'sass-loader' ]
      },
      {
        test: /\.html$/,
        use: [
          { loader: 'html-loader', options: { attrs: ['img:src'] } }
        ]
      },
      {
        test: /\.(png|jpg)$/,
        use: [
          { loader: 'file-loader', options: { name: 'images/[name]-[hash:8].[ext]' } }
        ]
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new htmlWebpackPlugin({
      template: "./src/index.html",
      inject: true
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("development"),
        WEBPACK: true
      }
    }),
    // new BundleAnalyzer({
    //   generateStatsFile: true
    // })
  ]
}